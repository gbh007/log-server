# Сервер для хранения логов

## [1.2.3] - 2021-05-06

**Добавлено:**

1. Пагинация на вкладке сообщений

**Изменено:**

1. Текст выбора вкладок кликабелен

## [1.2.2] - 2021-03-11

**Добавлено:**

1. Возможность настроить внешний вид
   - Фона страницы
   - Цвета фона информации (таблицы к диаграммам)
   - Цвета текста информации (таблицы к диаграммам)

## [1.2.1] - 2021-02-24

**Изменено:**

1. Диаграммы в гибриде теперь круговые

## [1.2.0] - 2021-02-08

**Изменено:**

1. Рефрешер работает через пропс
2. Теперь не обязательно указывать параметры лога для сервера (в случае если нет необходимости получать информацию о сервере)

**Добавлено:**

1. Изменения фильтра через выбор данных на вкладке анализ
2. Сохранение настроек в локальном хранилише
3. Изменение цветовой схемы
4. Гибридный режим (таблица + диаграммы)
5. Работа с запросом из адресной строки (параметр фильтра `q`) при успользование метода `GET`

**Удалено:**

1. react-router
2. chart.js

## [1.1.0] - 2021-02-01

**Изменено:**

1. Значение фильтра автоматически применяются на запрос

**Добавлено:**

1. Веб интерфейс на React
2. Вкладка анализа данных
3. Автообновление страницы

**Удалено:**

1. Веб интерфейс на Vue, Web Form

## [1.0.0] - 2020-10-20

**Добавлено:**

1. Возможность сохранять запись
2. Возможность фильтровать записи
3. Веб интерфейс
   - На VueJS
   - На Web Form
4. Лицензия
5. README
