module gitlab.com/gbh007/log-server

go 1.15

require (
	gitlab.com/gbh007/gojlog v1.2.1
	go.mongodb.org/mongo-driver v1.4.1
)
