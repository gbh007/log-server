package main

import (
	"flag"
	"fmt"

	"gitlab.com/gbh007/gojlog"
	"gitlab.com/gbh007/log-server/config"
	"gitlab.com/gbh007/log-server/mongodb"
	"gitlab.com/gbh007/log-server/web"
)

func main() {
	configPath := flag.String("c", "config.json", "файл конфигурации")
	staticDir := flag.String("s", "static", "папка с файлами для раздачи веб сервера")
	webPort := flag.Int("p", 80, "порт веб сервера")
	flag.Parse()
	gojlog.SetTrimPathAuto()
	err := config.Load(*configPath)
	if err != nil {
		gojlog.Critical("ошибка загрузки конфигурации, выход")
		return
	}
	gojlog.Info("завершение загрузки конфигурации")
	gojlog.Warning("запуск сервера логов")
	cnf := config.Get()
	err = mongodb.Connect(cnf.MongoConfig.URI, cnf.MongoConfig.DBName, cnf.MongoConfig.User, cnf.MongoConfig.Password)
	if err != nil {
		gojlog.Critical("ошибка подключения к базе, выход")
		return
	}
	gojlog.Info("завершение подключения к базе")
	done := web.Run(fmt.Sprintf(":%d", *webPort), *staticDir)
	gojlog.Info("завершение запуска веб сервера")
	gojlog.Warning("сервер логов запущен")
	<-done
	gojlog.Warning("сервер логов остановлен")
}
