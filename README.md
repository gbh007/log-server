# Сервер для хранения логов

совместим с [gojlog](https://gitlab.com/gbh007/gojlog)

## Возможности

- Сохранение записи json формата
- Просмотр записей с фильтрацией
  - По отправителю : совпадение/регулярное выражение
  - По месту возникновения : регулярное выражение
  - По времени : диапазон
  - По сообщению : регулярное выражение
  - По уровню : совпадение

## Пример конфигурационого файла

```json
{
  "mongo": {
    "user": "admin",
    "password": "1",
    "dbname": "log-server",
    "uri": "mongodb://localhost:27017"
  },
  "log": {
    "error_inform": false,
    "std": {
      "enable": true,
      "raw": true
    },
    "file": {
      "enable": true,
      "raw": true,
      "filename": ".hidden/log.txt"
    },
    "web": {
      "enable": true,
      "url": "http://localhost:8080/new?author=log-server"
    }
  }
}
```

## Примеры запросов

получение 50 последних сообщений с 2020 года содержащих в отправителе подстроку `test`

```http
POST http://localhost:8080/api/message/filter  HTTP/1.1

{
    "time":{
        "min":"2020-01-01T00:00:00Z"
    },
    "author_regexp":"test",
    "page":{
        "count":50,
        "reverse":true
    }
}
```

создание нового информационого сообщения

```http
POST http://localhost:8080/api/message/new  HTTP/1.1

{
    "time":"2020-01-02T03:04:05.000Z",
    "from":"http:0",
    "author":"test-http",
    "level":"info",
    "message":"информация"
}
```
