package config

import (
	"encoding/json"
	"os"

	"gitlab.com/gbh007/gojlog"
)

// Config структура для хранения конфигурации приложения
type Config struct {
	MongoConfig struct {
		URI      string `json:"uri"`
		User     string `json:"user"`
		Password string `json:"password"`
		DBName   string `json:"dbname"`
	} `json:"mongo"`
	LogConfig gojlog.LogConfig `json:"log,omitempty"`
}

var _config Config

// Get возврашает объект конфигурации
func Get() Config {
	return _config
}

// Load загружает конфиг
func Load(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	d := json.NewDecoder(file)
	if err := d.Decode(&_config); err != nil {
		gojlog.Error(err)
		return err
	}
	file.Close()
	gojlog.SetConfig(_config.LogConfig)
	return nil
}
