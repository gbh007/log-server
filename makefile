all: build

build:
	go build -o main

build-alpine:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main

run: build
	./main -p 8080 -c .hidden/config.json -s forntend/build

react-install:
	cd frontend && npm install

react-build:
	cd frontend && npm run build

react-run: go-build react-build
	./main -p 8080 -c .hidden/config.json -s frontend/build

start:
	cd frontend && npm start

docker: react-build build-alpine
	docker tag log-server:latest log-server:old || true
	docker build -t log-server:latest .
	docker stop log-server || true && docker rm log-server || true
	docker run -p 9250:80 -d --name log-server --restart always -v /etc/log-server:/app/cnf -v /var/log/log-server:/app/log log-server:latest
	docker rmi log-server:old || true