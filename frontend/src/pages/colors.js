function getRandomColor() {
  let letters = "0123456789ABCDEF".split("");
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

export function ChoiseName(code) {
  switch (code) {
    case "info":
      return "информация";
    case "warning":
      return "предупреждение";
    case "error":
      return "ошибка";
    case "critical":
      return "критический сбой";
    default:
      return "???";
  }
}

export function ChoiseColor(scheme, code) {
  return scheme.colors[code].background || "white";
}

let colors = {};

export function GetColor(name) {
  if (colors[name]) {
    return colors[name];
  }
  const color = getRandomColor();
  colors[name] = color;
  return color;
}
