import { Paginator } from "./filter";

export function LogView(props) {
  return (
    <div>
      <Paginator
        info={props.pageInfo}
        get={props.get}
        value={props.page}
        onChange={(e) => props.onChange(e)}
        height={8}
      />
      <LogTable
        data={props.data}
        colorScheme={props.colorScheme}
        theme={props.theme}
      />
    </div>
  );
}

export function LogTable(props) {
  return (
    <table className="list">
      <thead>
        <tr style={props.colorScheme.colors.head}>
          <td title="Номер на странице">#</td>
          <td title="Время генерации сообщения">Время</td>
          <td title="Отправитель сообщения">Источник</td>
          <td title="Адрес возникновения сообщения: файл и номер строки">
            Строка в коде
          </td>
          <td title="Текст сообщения">Сообщение</td>
        </tr>
      </thead>
      <tbody>
        {props.data.map((row, i) => (
          <LogRow key={i} {...row} i={i} colorScheme={props.colorScheme} />
        ))}
      </tbody>
    </table>
  );
}

function toLocalTime(t) {
  return new Date(t).toLocaleString();
}

function LogRow(props) {
  return (
    <tr title={props.level} style={props.colorScheme.colors[props.level]}>
      <td style={{ background: "white", color: "black" }}>{props.i + 1}</td>
      <td>{toLocalTime(props.time)}</td>
      <td>
        <b>{props.author}</b>
      </td>
      <td>{props.from}</td>
      <td>{props.message}</td>
    </tr>
  );
}
