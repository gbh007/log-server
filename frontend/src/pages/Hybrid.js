import React from "react";
import { ChoiseColor, ChoiseName, GetColor } from "./colors";
import { LogTable } from "./LogTable";

export class Hybrid extends React.Component {
  render() {
    return (
      <div>
        <div style={{ width: "70%", float: "left" }}>
          <LogTable
            data={this.props.data}
            colorScheme={this.props.colorScheme}
          />
        </div>
        <div style={{ width: "30%", float: "left" }}>
          <div style={{ width: "100%", float: "left" }}>
            <CustomPie
              data={this.props.mt}
              isLevel={true}
              style={{ padding: "10px" }}
              limit={30}
              onChange={(e) => this.props.setFilter("level", e)}
              colorScheme={this.props.colorScheme}
              theme={this.props.theme}
            />
            <CustomPie
              data={this.props.ma}
              style={{ padding: "10px" }}
              limit={30}
              onChange={(e) => this.props.setFilter("author", e)}
              colorScheme={this.props.colorScheme}
              theme={this.props.theme}
            />
            <CustomPie
              data={this.props.mf}
              style={{ padding: "10px" }}
              limit={30}
              onChange={(e) => this.props.setFilter("from", e)}
              colorScheme={this.props.colorScheme}
              theme={this.props.theme}
            />
          </div>
        </div>
      </div>
    );
  }
}

class CustomPie extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  getColor(name) {
    if (this.props.isLevel) {
      return ChoiseColor(this.props.colorScheme, name);
    }
    return GetColor(name);
  }
  componentDidMount() {
    this.parseData(this.props.data);
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.data !== this.props.data ||
      prevProps.isLevel !== this.props.isLevel ||
      prevProps.colorScheme !== this.props.colorScheme
    ) {
      this.parseData(this.props.data);
    }
  }
  parseData(data) {
    // тип диаграммы уровень сообшения (тип)
    const isLevel = this.props.isLevel;
    // суммарное значение количества
    let all = 0;
    // данные, присвоение массива если необъявлены
    data = data || [];
    // подготовка данных
    data = Object.entries(data).map((e) => {
      return { name: e[0], count: e[1] };
    });
    // удаление для уровней сообщения значения "ВСЕ"
    if (isLevel) {
      data = data.filter((e) => e.name !== "all");
    }
    // удаление пустых значений
    data = data.filter((e) => e.count > 0);
    // сортировка по убыванию
    data.sort((a, b) => b.count - a.count);
    // подсчет суммарного значения
    data.forEach((e) => {
      all += e.count;
    });
    // сдвиг области на круге
    let offset = 0;
    // расчет данных для отображения
    data = data.map((e) => {
      e.offset = 100 + 25 - offset;
      const dash = (e.count * 100) / all;
      e.dash = "" + dash + " " + (100 - dash);
      offset += dash;
      // предварительный расчет, посже будут пересчитаны
      e.color = this.getColor(e.name);
      e.display_name = isLevel ? ChoiseName(e.name) : e.name;
      return e;
    });
    this.setState({ data: data });
  }
  render() {
    // получение пропсов
    const isLevel = this.props.isLevel;
    const onChange = this.props.onChange || (() => {});
    // окраска данных
    const data = this.state.data.map((e) => {
      e.color = this.getColor(e.name);
      e.display_name = isLevel ? ChoiseName(e.name) : e.name;
      return e;
    });
    return (
      <div
        style={{
          textAlign: "center",
          display: "flex",
          alignItems: "center",
        }}
      >
        <svg viewBox="0 0 42 42" role="img" style={{ width: "40%" }}>
          {data.map((e) => (
            <circle
              key={e.name}
              cx="21"
              cy="21"
              r="15.91549430918954"
              fill="transparent"
              stroke={e.color}
              strokeWidth="3"
              strokeDasharray={e.dash}
              strokeDashoffset={e.offset}
              onClick={() => onChange(e.name)} // не работает корректно
            >
              <title>{e.display_name + " " + e.count}</title>
            </circle>
          ))}
          <text
            x="50%"
            y="55%"
            style={{
              textAnchor: "middle",
              fontSize: "40%",
              verticalAlign: "middle",
            }}
          >
            {(data.length > 10 ? 10 : data.length) + "/" + data.length}
          </text>
        </svg>
        <table
          style={{
            ...this.props.style,
            width: "60%",
            borderSpacing: "0px",
            display: "inline-block",
            background: this.props.theme.info_background,
            color: this.props.theme.info_color,
            borderRadius: "10px",
          }}
        >
          <tbody style={{ fontSize: "normal" }}>
            {data.map((e, ind) => {
              if (ind >= 10) return null;
              return (
                <tr
                  key={e.name + e.color}
                  title={e.count}
                  onClick={() => onChange(e.name)}
                >
                  <td
                    style={{
                      whiteSpace: "nowrap",
                      textAlign: "right",
                    }}
                  >
                    {e.count}
                  </td>
                  <td
                    style={{
                      background: e.color,
                      display: "inline-block",
                      width: "10px",
                      height: "10px",
                      borderRadius: "5px",
                    }}
                  ></td>
                  <td
                    style={{
                      textAlign: "left",
                      wordBreak: "break-all",
                    }}
                  >
                    {e.display_name}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
