import React from "react";
import { ChoiseColor, ChoiseName, GetColor } from "./colors";

export class Analyze extends React.Component {
  render() {
    return (
      <div>
        <div style={{ width: "30%", float: "left" }}>
          <CustomBar
            data={this.props.mt}
            style={{ padding: "10px" }}
            onChange={(e) => this.props.setFilter("level", e)}
            isLevel={true}
            colorScheme={this.props.colorScheme}
            theme={this.props.theme}
          />
          <CustomBar
            data={this.props.ma}
            style={{ padding: "10px" }}
            onChange={(e) => this.props.setFilter("author", e)}
            colorScheme={this.props.colorScheme}
            theme={this.props.theme}
          />
        </div>
        <div style={{ width: "70%", float: "left" }}>
          <CustomBar
            data={this.props.mf}
            style={{ padding: "10px" }}
            onChange={(e) => this.props.setFilter("from", e)}
            colorScheme={this.props.colorScheme}
            theme={this.props.theme}
          />
        </div>
      </div>
    );
  }
}

export class CustomBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
    this.getColor = this.getColor.bind(this);
  }
  getColor(name) {
    if (this.props.isLevel) {
      return ChoiseColor(this.props.colorScheme, name);
    }
    return GetColor(name);
  }
  componentDidMount() {
    this.parseData(this.props.data);
  }
  componentDidUpdate(prevProps) {
    if (
      prevProps.data !== this.props.data ||
      prevProps.isLevel !== this.props.isLevel ||
      prevProps.colorScheme !== this.props.colorScheme
    ) {
      this.parseData(this.props.data);
    }
  }
  parseData(data) {
    // тип диаграммы уровень сообшения (тип)
    const isLevel = this.props.isLevel;
    // максимальное значение количества
    let max = 0;
    // данные, присвоение массива если необъявлены
    data = data || [];
    // подготовка данных
    data = Object.entries(data).map((e) => {
      return { name: e[0], count: e[1] };
    });
    // удаление для уровней сообщения значения "ВСЕ"
    if (isLevel) {
      data = data.filter((e) => e.name !== "all");
    }
    // удаление пустых значений
    data = data.filter((e) => e.count > 0);
    // сортировка по убыванию
    data.sort((a, b) => b.count - a.count);
    // подсчет максимального значения
    data.forEach((e) => {
      max = Math.max(max, e.count);
    });
    // расчет данных для отображения
    data = data.map((e) => {
      // предварительный расчет, посже будут пересчитаны
      e.color = this.getColor(e.name);
      e.display_name = isLevel ? ChoiseName(e.name) : e.name;
      e.part = (e.count * 100) / max;
      return e;
    });
    this.setState({ data: data });
  }
  render() {
    // получение пропсов
    const isLevel = this.props.isLevel;
    const onChange = this.props.onChange || (() => {});
    // окраска данных
    let data = this.state.data.map((e) => {
      e.color = this.getColor(e.name);
      e.display_name = isLevel ? ChoiseName(e.name) : e.name;
      return e;
    });
    // подсчет количества пропущщеных (неотображаемых) и фильтрация по количеству
    let skipped = data.length;
    skipped -= data.length;
    return (
      <table
        style={{
          ...this.props.style,
          width: "100%",
          borderSpacing: "0px",
          display: "inline-block",
          background: this.props.theme.info_background,
          color: this.props.theme.info_color,
          borderRadius: "10px",
        }}
      >
        <tbody style={{ fontSize: "normal" }}>
          {data.map((e) => (
            <tr key={e.name} title={e.count} onClick={() => onChange(e.name)}>
              <td
                style={{
                  whiteSpace: "nowrap",
                  textAlign: "right",
                  paddingRight: "10px",
                }}
              >
                {e.display_name}
              </td>
              <td style={{ width: "100%" }}>
                <div>
                  <div
                    style={{
                      width: e.part + "%",
                      background: e.color,
                      height: "10px",
                    }}
                  ></div>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
        {skipped > 0 ? (
          <tfoot>
            <tr>
              <td
                style={{
                  whiteSpace: "nowrap",
                  color: "red",
                  textAlign: "right",
                  paddingRight: "10px",
                }}
              >
                пропущено
              </td>
              <td>{skipped}</td>
            </tr>
          </tfoot>
        ) : null}
      </table>
    );
  }
}
