import React from "react";
import "./paginator.css";

// пагинатор (панель с номерами страниц)
export class Paginator extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      use: false,
      defaultValue: props.value,
    };
    this.reset = this.reset.bind(this);
    this.setOffset = this.setOffset.bind(this);
    this.setReverse = this.setReverse.bind(this);
    this.setCount = this.setCount.bind(this);
  }
  reset() {
    this.props.onChange(this.state.defaultValue);
    this.setState({ use: false, show: false });
  }
  update(fieldName, value) {
    let tmp = { ...this.props.value };
    tmp[fieldName] = value;
    this.props.onChange(tmp);
  }
  setOffset(offset) {
    this.update("offset", offset);
  }
  setReverse() {
    this.update("reverse", !this.props.value.reverse);
    this.setState({ use: true });
  }
  setCount(e) {
    this.update("count", parseInt(e.target.value));
    this.setState({ use: true });
  }
  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.props.get();
    }
  }
  calcPages(cur, max, count) {
    const leftPageCount = 3;
    const middlePageCount = 5;
    const rightPageCount = 3;
    const len = leftPageCount + middlePageCount + rightPageCount + 2;
    let pages = [];
    if (max <= len) {
      for (let i = 0; i < max; i++) {
        pages.push({
          page: i + 1,
          offset: i * count,
        });
      }
      return pages;
    }
    // генерация первой 3-ки страниц
    for (let i = 0; i < leftPageCount; i++) {
      pages.push({
        page: i + 1,
        offset: i * count,
      });
    }
    // генерация центральных страниц которые близко к первым 3-м страницам
    if (cur < leftPageCount + middlePageCount) {
      for (let i = leftPageCount; i < leftPageCount + middlePageCount; i++) {
        pages.push({
          page: i + 1,
          offset: i * count,
        });
      }
      // добавляем разделитель
      pages.push({
        page: -1,
        offset: 0,
      });
    }
    // генерация центральных страниц которые близко к последним 3-м страницам
    if (cur > max + 1 - middlePageCount - rightPageCount) {
      // добавляем разделитель
      pages.push({
        page: -1,
        offset: 0,
      });
      for (
        let i = max - middlePageCount - rightPageCount;
        i < max - rightPageCount;
        i++
      ) {
        pages.push({
          page: i + 1,
          offset: i * count,
        });
      }
    }
    // генерация центральных страниц в нормальном случае
    if (
      cur >= leftPageCount + middlePageCount &&
      cur <= max + 1 - middlePageCount - rightPageCount
    ) {
      // добавляем разделитель
      pages.push({
        page: -1,
        offset: 0,
      });
      for (
        let i = cur - Math.ceil(middlePageCount / 2);
        i < cur + Math.floor(middlePageCount / 2);
        i++
      ) {
        pages.push({
          page: i + 1,
          offset: i * count,
        });
      }
      // добавляем разделитель
      pages.push({
        page: -1,
        offset: 0,
      });
    }
    // генерация последней тройки страниц
    for (let i = max - rightPageCount; i < max; i++) {
      pages.push({
        page: i + 1,
        offset: i * count,
      });
    }
    return pages;
  }
  render() {
    const pageInfo = this.props.info;
    if (pageInfo == null) {
      return null;
    }
    const pages = this.calcPages(
      pageInfo.current,
      pageInfo.count,
      this.props.value.count
    );
    return (
      <div className="paginator">
        {pages.map((e, ind) => (
          <Page
            page={e.page}
            offset={e.offset}
            key={e.offset + "" + ind}
            select={e.page === pageInfo.current}
            empty={e.page === -1}
            onChange={this.setOffset}
          />
        ))}
        {this.props.children}
      </div>
    );
  }
}
Paginator.defaultProps = {
  height: 10,
};

// функция рендера номера страницы
function Page(props) {
  const type = props.select ? "selected" : props.empty ? "empty" : "";
  return (
    <div
      className="page"
      onClick={() => (props.page > 0 ? props.onChange(props.offset) : null)}
      type={type}
    >
      {props.page > 0 ? props.page : "..."}
    </div>
  );
}
