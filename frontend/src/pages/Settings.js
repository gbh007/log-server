import React from "react";
import { DefaultColorScheme } from "../style";

export class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultMenu: JSON.parse(localStorage.getItem("ls-menu")) || "list",
    };
  }
  saveSettings() {
    localStorage.setItem("ls-filter", JSON.stringify(this.props.filter));
    localStorage.setItem("ls-shift", JSON.stringify(this.props.shift));
    localStorage.setItem("ls-menu", JSON.stringify(this.state.defaultMenu));
    localStorage.setItem("ls-refresh", JSON.stringify(this.props.refresh));
    localStorage.setItem(
      "ls-color-scheme",
      JSON.stringify(this.props.colorScheme)
    );
    localStorage.setItem("ls-theme", JSON.stringify(this.props.theme));
    alert("сохранено");
  }
  delSettings() {
    localStorage.removeItem("ls-filter");
    localStorage.removeItem("ls-shift");
    localStorage.removeItem("ls-menu");
    localStorage.removeItem("ls-refresh");
    localStorage.removeItem("ls-color-scheme");
    localStorage.removeItem("ls-theme");
    alert("удалено");
  }
  setColorScheme(code) {
    for (let e of DefaultColorScheme) {
      if (e.code === code) {
        this.props.setColorScheme(e);
      }
    }
  }
  updateColorScheme(level, tp, value) {
    const scheme = this.props.colorScheme;
    let tmp = {
      ...scheme,
      code: "custom",
      name: "Своя",
      colors: {
        ...scheme.colors,
        [level]: { ...scheme.colors[level], [tp]: value },
      },
    };
    this.props.setColorScheme(tmp);
  }
  render() {
    const cs = this.props.colorScheme;
    const messageLevels = [
      { code: "head", name: "Заголовок таблицы" },
      { code: "info", name: "Информация" },
      { code: "warning", name: "Предупреждение" },
      { code: "error", name: "Ошибка" },
      { code: "critical", name: "Сбой" },
    ];
    return (
      <div>
        <div style={{ marginTop: "20px" }}>
          Страница по умолчанию
          <select
            value={this.state.defaultMenu}
            onChange={(e) => this.setState({ defaultMenu: e.target.value })}
            style={{ marginLeft: "10px" }}
          >
            <option value="list">Сообщения</option>
            <option value="analyze">Анализ</option>
            <option value="hybrid">Гибрид</option>
          </select>
        </div>
        <div style={{ marginTop: "20px" }}>
          Цветовая схема
          <select
            value={cs.code}
            onChange={(e) => this.setColorScheme(e.target.value)}
            style={{ marginLeft: "10px" }}
          >
            {DefaultColorScheme.map((e) => (
              <option value={e.code} key={e.code}>
                {e.name}
              </option>
            ))}
            <option value="custom" hidden={true}>
              Своя
            </option>
          </select>
          <table style={{ marginTop: "10px" }} className="sp">
            <thead>
              <tr>
                <td>Образец</td>
                <td>Цвет фона</td>
                <td>Цвет текста</td>
              </tr>
            </thead>
            <tbody>
              {messageLevels.map((lv) => (
                <tr style={cs.colors[lv.code]} key={lv.code}>
                  <td>{lv.name}</td>
                  <td>
                    <input
                      type="color"
                      value={cs.colors[lv.code].background}
                      onChange={(e) =>
                        this.updateColorScheme(
                          lv.code,
                          "background",
                          e.target.value
                        )
                      }
                    />
                  </td>
                  <td>
                    <input
                      type="color"
                      value={cs.colors[lv.code].color}
                      onChange={(e) =>
                        this.updateColorScheme(lv.code, "color", e.target.value)
                      }
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div style={{ marginTop: "20px" }}>
          <Theme
            theme={this.props.theme}
            onChange={(e) => this.props.setTheme(e)}
          />
        </div>
        <div style={{ marginTop: "20px" }}>
          <button onClick={() => this.saveSettings()} className="green">
            Сохранить
          </button>
          <button
            onClick={() => this.delSettings()}
            style={{ color: "red" }}
            className="red"
          >
            Удалить
          </button>
        </div>
      </div>
    );
  }
}

function Theme(props) {
  const background = props.theme.info_background.slice(0, 7);
  const transparent = parseInt(props.theme.info_background.slice(7, 9), 16);
  const update = (fieldName, value) => {
    props.onChange({ ...props.theme, [fieldName]: value });
  };
  return (
    <table>
      <tbody>
        <tr>
          <td>Фон</td>
          <td>
            <input
              value={props.theme.background}
              onChange={(e) => update("background", e.target.value)}
            />
          </td>
        </tr>
        <tr>
          <td>Фон информации</td>
          <td>
            <input
              type="color"
              value={background}
              onChange={(e) =>
                update(
                  "info_background",
                  e.target.value + transparent.toString(16)
                )
              }
            />
          </td>
        </tr>
        <tr>
          <td>Прозрачность фона информации</td>
          <td>
            <input
              type="range"
              value={transparent}
              min={0}
              max={255}
              onChange={(e) =>
                update(
                  "info_background",
                  background + parseInt(e.target.value).toString(16)
                )
              }
            />
          </td>
        </tr>
        <tr>
          <td>Цвет текста информации</td>
          <td>
            <input
              type="color"
              value={props.theme.info_color}
              onChange={(e) => update("info_color", e.target.value)}
            />
          </td>
        </tr>
      </tbody>
    </table>
  );
}
