import React from "react";
import axios from "axios";
import { LogView } from "./pages/LogTable.js";
import { Filter } from "./header/Filter.js";
import { Refresher } from "./header/Refresher.js";
import { Menu } from "./header/Menu.js";
import { Analyze } from "./pages/Analyze.js";
import { Settings } from "./pages/Settings.js";
import { Hybrid } from "./pages/Hybrid.js";

export class App extends React.Component {
  constructor(props) {
    super(props);
    let tmp = this.cleanFilter();
    this.state = {
      filter: tmp.filter,
      data: [],
      page: null,
      informer: {
        info: 0,
        warning: 0,
        error: 0,
        critical: 0,
        all: 1,
      },
      authors: [],
      ma: [],
      mf: [],
      shift: tmp.shift,
      menu: tmp.menu,
      refresh: tmp.refresh,
      colorScheme: tmp.colorScheme,
      theme: tmp.theme,
    };
    this.get = this.get.bind(this);
    this.setFilter = this.setFilter.bind(this);
    this.setShift = this.setShift.bind(this);
    this.resetFilter = this.resetFilter.bind(this);
  }
  componentDidMount() {
    this.get();
  }
  componentDidUpdate(_, prevState) {
    if (
      this.state.filter !== prevState.filter ||
      this.state.shift !== prevState.shift
    ) {
      this.get();
    }
  }
  resetFilter() {
    this.setState(this.cleanFilter());
  }
  cleanFilter() {
    const shift = 24 * 60 * 60;
    return {
      filter: JSON.parse(localStorage.getItem("ls-filter")) || {
        from: "",
        author: "",
        level: "",
        message: "",
        page: {
          count: 20,
          reverse: true,
        },
      },
      shift: JSON.parse(localStorage.getItem("ls-shift")) || shift,
      menu: JSON.parse(localStorage.getItem("ls-menu")) || "list",
      refresh: JSON.parse(localStorage.getItem("ls-refresh")) || 60,
      colorScheme: JSON.parse(localStorage.getItem("ls-color-scheme")) || {
        name: "Сфетофор",
        code: "sem",
        colors: {
          head: { color: "#ffffff", background: "#063a52" },
          info: { color: "#000000", background: "#d3d3d3" },
          warning: { color: "#000000", background: "#d6a74f" },
          error: { color: "#000000", background: "#ffc0cb" },
          critical: { color: "#000000", background: "#c74a4a" },
        },
      },
      theme: JSON.parse(localStorage.getItem("ls-theme")) || {
        background: "#ffffff",
        info_background: "#ffffff8c",
        info_color: "#808080",
      },
    };
  }
  get() {
    let filter = { ...this.state.filter };
    if (this.state.shift > 0) {
      let timeMin = new Date();
      timeMin.setSeconds(timeMin.getSeconds() - this.state.shift, 0);
      filter.time = { min: timeMin.toJSON() };
    } else {
      filter.time = {};
    }
    axios
      .post("/api/message/filter", filter)
      .then((response) => {
        this.setState({ data: response.data.data });
        this.setState({ page: response.data.page });
        this.setState({ informer: response.data.mt });
        let authors = [];
        for (let key in response.data.ma) {
          authors.push({ name: key, count: response.data.ma[key] });
        }
        this.setState({ authors: authors });
        this.setState({ ma: response.data.ma });
        this.setState({ mf: response.data.mf });
      })
      .catch((err) => {
        console.log(err);
        alert(JSON.stringify(err));
      });
  }
  setShift(shift) {
    this.setState({ shift: shift });
  }
  setFilter(key, value) {
    this.setState((state) => ({
      filter: { ...state.filter, [key]: value },
    }));
  }
  render() {
    let noData =
      this.state.page == null
        ? false
        : this.state.data
        ? this.state.data.length < 1
        : true;
    return (
      <div>
        <div
          style={{
            background: this.state.theme.background,
            top: "0px",
            left: "0px",
            width: "100%",
            height: "100%",
            position: "fixed",
            zIndex: "-1",
          }}
        ></div>
        <Filter
          setFilter={this.setFilter}
          filter={this.state.filter}
          shift={this.state.shift}
          authors={this.state.authors}
          setShift={this.setShift}
        />
        <button onClick={this.resetFilter} className="filter red">
          Сбросить
        </button>
        <p className="filter">
          Всего:
          <b className="count">
            {this.state.page ? this.state.page.items_total : 0}
          </b>
        </p>
        <Refresher
          get={this.get}
          value={this.state.refresh}
          onChange={(e) => this.setState({ refresh: e })}
        />
        <Menu
          value={this.state.menu}
          onChange={(e) => this.setState({ menu: e })}
        />
        <hr />
        {noData && this.state.menu !== "settings" ? <h1>Данных нет</h1> : null}
        {!noData && this.state.menu === "analyze" ? (
          <Analyze
            setFilter={this.setFilter}
            ma={this.state.ma}
            mt={this.state.informer}
            mf={this.state.mf}
            colorScheme={this.state.colorScheme}
            theme={this.state.theme}
          />
        ) : null}
        {!noData && this.state.menu === "list" ? (
          <LogView
            pageInfo={this.state.page}
            get={this.get}
            page={this.state.filter.page}
            onChange={(e) => this.setFilter("page", e)}
            data={this.state.data}
            colorScheme={this.state.colorScheme}
            theme={this.state.theme}
          />
        ) : null}
        {!noData && this.state.menu === "hybrid" ? (
          <Hybrid
            data={this.state.data}
            setFilter={this.setFilter}
            ma={this.state.ma}
            mt={this.state.informer}
            mf={this.state.mf}
            colorScheme={this.state.colorScheme}
            theme={this.state.theme}
          />
        ) : null}
        {this.state.menu === "settings" ? (
          <Settings
            filter={this.state.filter}
            shift={this.state.shift}
            refresh={this.state.refresh}
            colorScheme={this.state.colorScheme}
            setColorScheme={(e) => this.setState({ colorScheme: e })}
            theme={this.state.theme}
            setTheme={(e) => this.setState({ theme: e })}
          />
        ) : null}
      </div>
    );
  }
}
