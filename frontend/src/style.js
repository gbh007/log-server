export const DefaultColorScheme = [
  {
    name: "Светофор",
    code: "sem",
    colors: {
      head: { color: "#ffffff", background: "#063a52" },
      info: { color: "#000000", background: "#d3d3d3" },
      warning: { color: "#000000", background: "#d6a74f" },
      error: { color: "#000000", background: "#ffc0cb" },
      critical: { color: "#000000", background: "#c74a4a" },
    },
  },
  {
    name: "Альтернативная",
    code: "alt",
    colors: {
      head: { color: "#ffffff", background: "#063a52" },
      info: { color: "#ffffff", background: "#b8c6c3" },
      warning: { color: "#ffffff", background: "#bcb076" },
      error: { color: "#ffffff", background: "#858c63" },
      critical: { color: "#ffffff", background: "#586776" },
    },
  },
  {
    name: "Пчела",
    code: "bee",
    colors: {
      critical: { color: "#ffffff", background: "#000000" },
      error: { color: "#000000", background: "#fbff00" },
      head: { color: "#ffffff", background: "#80d7ff" },
      info: { color: "#000000", background: "#dbdbdb" },
      warning: { color: "#ffffff", background: "#a57418" },
    },
  },
];
