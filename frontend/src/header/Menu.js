import React from "react";

export function Menu(props) {
  return (
    <div>
      <label>
        <input
          type="radio"
          checked={props.value === "list"}
          onChange={() => props.onChange("list")}
        />
        Сообщения
      </label>
      <label>
        <input
          type="radio"
          checked={props.value === "analyze"}
          onChange={() => props.onChange("analyze")}
        />
        Анализ
      </label>
      <label>
        <input
          type="radio"
          checked={props.value === "hybrid"}
          onChange={() => props.onChange("hybrid")}
        />
        Гибрид
      </label>
      <label>
        <input
          type="radio"
          checked={props.value === "settings"}
          onChange={() => props.onChange("settings")}
        />
        Настройки
      </label>
    </div>
  );
}
