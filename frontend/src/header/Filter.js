export function Filter(props) {
  return (
    <div style={{ display: "inline" }}>
      <input
        className="filter"
        type="text"
        placeholder="Строка в коде"
        title="Строка в коде"
        value={props.filter.from}
        onChange={(e) => props.setFilter("from", e.target.value)}
      />
      <select
        className="filter"
        value={props.filter.author}
        title="Источник"
        onChange={(e) => props.setFilter("author", e.target.value)}
      >
        {props.authors.map((author) => (
          <option key={author.name} value={author.name}>
            {author.name + " (" + author.count + ")"}
          </option>
        ))}
        <option value="">все</option>
      </select>
      <select
        className="filter"
        value={props.filter.level}
        title="Тип"
        onChange={(e) => props.setFilter("level", e.target.value)}
      >
        <option value={"info"}>информация</option>
        <option value={"warning"}>предупреждение</option>
        <option value={"error"}>ошибка</option>
        <option value={"critical"}>критический сбой</option>
        <option value={""}>все</option>
      </select>
      <input
        className="filter"
        placeholder="Сообщение"
        title="Сообщение"
        value={props.filter.message}
        onChange={(e) => props.setFilter("message", e.target.value)}
      />
      <select
        className="filter"
        value={props.filter.page.count}
        title="Сколько"
        onChange={(e) =>
          props.setFilter("page", {
            count: parseInt(e.target.value),
            reverse: true,
          })
        }
      >
        <option value={10}>10</option>
        <option value={20}>20</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
        <option value={-1}>все</option>
      </select>
      <select
        className="filter"
        value={props.shift}
        title="Период времени"
        onChange={(e) => props.setShift(parseInt(e.target.value))}
      >
        <option value={10 * 60}>10 минут</option>
        <option value={1 * 60 * 60}>1 час</option>
        <option value={2 * 60 * 60}>2 часа</option>
        <option value={4 * 60 * 60}>4 часа</option>
        <option value={6 * 60 * 60}>6 часов</option>
        <option value={12 * 60 * 60}>12 часов</option>
        <option value={24 * 60 * 60}>24 часа</option>
        <option value={7 * 24 * 60 * 60}>7 дней</option>
        <option value={14 * 24 * 60 * 60}>14 дней</option>
        <option value={28 * 24 * 60 * 60}>28 дней</option>
        <option value={-1}>все</option>
      </select>
    </div>
  );
}
