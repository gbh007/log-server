import React from "react";

export class Refresher extends React.Component {
  constructor(props) {
    super(props);
    this.id = null;
    this.resetRefresh = this.resetRefresh.bind(this);
  }
  componentDidMount() {
    this.resetRefresh();
  }
  componentWillUnmount() {
    if (this.id != null) {
      clearInterval(this.id);
    }
  }
  componentDidUpdate(prevProp) {
    if (this.props.value !== prevProp.value) {
      this.resetRefresh();
    }
  }
  resetRefresh() {
    if (this.id != null) {
      clearInterval(this.id);
    }
    if (this.props.value > 0) {
      this.id = setInterval(this.props.get, this.props.value * 1000);
    }
  }
  render() {
    return (
      <div style={{ display: "inline" }} className="filter">
        <select
          className="filter"
          value={this.props.value}
          title="период времени автообновления"
          onChange={(e) => this.props.onChange(parseInt(e.target.value))}
        >
          <option value={1}>1 секунда</option>
          <option value={5}>5 секунд</option>
          <option value={10}>10 секунд</option>
          <option value={30}>30 секунд</option>
          <option value={60}>60 секунд</option>
          <option value={5 * 60}>5 минут</option>
          <option value={10 * 60}>10 минут</option>
          <option value={20 * 60}>20 минут</option>
        </select>
      </div>
    );
  }
}
