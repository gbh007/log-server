package web

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/gbh007/gojlog"
)

// перменные ошибок
var (
	ErrForbidden      = fmt.Errorf("нет прав доступа")
	ErrNotFound       = fmt.Errorf("данные не найдены")
	ErrParseData      = fmt.Errorf("некорректный формат данных")
	ErrDeleteData     = fmt.Errorf("невозможно удалить данные")
	ErrCreateData     = fmt.Errorf("невозможно создать данные")
	ErrAppendData     = fmt.Errorf("невозможно добавить данные")
	ErrUpdateData     = fmt.Errorf("невозможно изменить данные")
	ErrProcessingData = fmt.Errorf("невозможно обработать данные")
	ErrNotAuth        = fmt.Errorf("необходима авторизация")
	ErrData           = fmt.Errorf("некорректные данные")
)

// ParseJSON читает из тела запроса переданную структуру
func ParseJSON(r *http.Request, data interface{}) error {
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&data)
	if err != nil && err != io.EOF {
		gojlog.Error(err)
	}
	return err
}

type contextKey int

// ключи для ResponseContext
const (
	ResponseDataKey contextKey = iota
)

// ResponseContext контекст для ответа
type ResponseContext struct {
	context.Context
	err error
	key interface{}
	val interface{}
}

// Err возврашает ошибку если не nil иначе ошибку родителя
func (rc *ResponseContext) Err() error {
	if rc.err != nil {
		return rc.err
	}
	return rc.Context.Err()
}

// Value возврашает значение контекста по ключу
// если в данном контексте нет то ищет у родителя
func (rc *ResponseContext) Value(key interface{}) interface{} {
	if key == rc.key {
		return rc.val
	}
	return rc.Context.Value(key)
}

// WithResponseContext возвращает копию родителя,
// если ошибка не nil то вернет ошибку родителя,
// возврашает поле data по ключу key
func WithResponseContext(parent context.Context, key, val interface{}, err error) context.Context {
	return &ResponseContext{
		Context: parent,
		err:     err,
		key:     key,
		val:     val,
	}
}

// SetError устанавливает в контекст ошибку
func SetError(r *http.Request, err error) {
	*r = *r.WithContext(
		WithResponseContext(
			r.Context(),
			ResponseDataKey,
			nil,
			err,
		),
	)
}

// SetResponse устанавливает в контекст json ответ
func SetResponse(r *http.Request, data interface{}) {
	*r = *r.WithContext(
		WithResponseContext(
			r.Context(),
			ResponseDataKey,
			data,
			nil,
		),
	)
}

// JSONWriteHandler хандлер для ответа в виде json
func JSONWriteHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
		w.Header().Set("Content-Type", "application/json")
		if r.Method == http.MethodOptions {
			w.Header().Set("Allow", "POST, OPTIONS")
			return
		}
		if next != nil {
			next.ServeHTTP(w, r)
		}
		enc := json.NewEncoder(w)
		if err := r.Context().Err(); err != nil {
			switch err {
			case ErrForbidden:
				w.WriteHeader(http.StatusForbidden)
			case ErrNotFound:
				w.WriteHeader(http.StatusNotFound)
			case ErrNotAuth:
				w.WriteHeader(http.StatusUnauthorized)
			default:
				w.WriteHeader(http.StatusInternalServerError)
			}
			if err := enc.Encode(err.Error()); err != nil {
				gojlog.Error(err)
			}
			return
		}
		w.WriteHeader(http.StatusOK)
		data := r.Context().Value(ResponseDataKey)
		if data == nil {
			return
		}
		if err := enc.Encode(data); err != nil {
			gojlog.Error(err)
		}
	})
}
