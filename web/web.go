package web

import (
	"net/http"
	"time"

	"gitlab.com/gbh007/gojlog"
)

// Run запускает веб сервер
func Run(addr string, staticDir string) <-chan struct{} {
	mux := http.NewServeMux()
	// обработчик статики
	mux.Handle("/", http.FileServer(http.Dir(staticDir)))
	// обработчики сообщений
	mux.Handle("/new", JSONWriteHandler(CreateMessage()))
	mux.Handle("/api/message/new", JSONWriteHandler(CreateMessage()))
	mux.Handle("/api/message/filter", JSONWriteHandler(FilteredMessages()))
	// создание объекта сервера
	server := &http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
		IdleTimeout:  1 * time.Minute,
	}
	done := make(chan struct{})
	go func() {
		if err := server.ListenAndServe(); err != nil {
			gojlog.Error(err)
		}
		close(done)
	}()
	return done
}
