package web

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gbh007/log-server/mongodb"
)

// CreateMessage сохраняет новое сообщение лога
func CreateMessage() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := mongodb.LogMessage{}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		// только для автора сообщения проверяем в урле
		// необходимо для совместимости с gojlog
		if req.Author == "" {
			req.Author = r.URL.Query().Get("author")
		}
		id, err := mongodb.CreateMessage(req)
		if err != nil {
			SetError(r, err)
		} else {
			SetResponse(r, id)
		}
	})
}

// FilteredMessages возвращает список сообщений по фильтру
func FilteredMessages() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := mongodb.MessageFilter{}
		if r.Method == http.MethodGet {
			q := r.URL.Query().Get("q")
			if json.Unmarshal([]byte(q), &req) == nil {
				messages, page := mongodb.FilteredMessages(req)
				SetResponse(r, struct {
					Page mongodb.PageInfo `json:"page"`
					Data []mongodb.LogMessage `json:"data"`
				}{
					Page: page,
					Data: messages,
				})
				return
			}
		}
		if ParseJSON(r, &req) != nil {
			SetError(r, ErrParseData)
			return
		}
		messages, page := mongodb.FilteredMessages(req)
		mt := mongodb.CalcMT(req)
		ma := mongodb.CalcMA(req)
		mf := mongodb.CalcMF(req)
		SetResponse(r, struct {
			Page mongodb.PageInfo       `json:"page"`
			MT   mongodb.MessageTypes   `json:"mt"`
			MA   mongodb.MessageAuthors `json:"ma"`
			MF   mongodb.MessageFrom    `json:"mf"`
			Data []mongodb.LogMessage   `json:"data"`
		}{
			Page: page,
			MT:   mt,
			MA:   ma,
			MF:   mf,
			Data: messages,
		})
	})
}
