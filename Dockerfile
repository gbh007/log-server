FROM amd64/alpine:3.13

LABEL maintainer="Никониров Григорий mrgbh007@gmail.com"

# добавление ssl сертификатов и пакета временых зон
RUN apk update && apk add ca-certificates tzdata
WORKDIR /app
VOLUME /app/cnf
VOLUME /app/log
COPY main /app/
COPY frontend/build /app/static
EXPOSE 80
ENTRYPOINT ["/app/main", "-c", "/app/cnf/config.json"]
