package mongodb

import (
	"context"
	"time"

	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var _mongoDB *mongo.Database

// Connect подключается к основной бд
func Connect(uri, dbName, user, password string) error {
	ctx, cnl := NewTimeoutContext(nil)
	defer cnl()
	opts := options.Client().ApplyURI(
		uri,
	)
	if user != "" {
		opts.SetAuth(
			options.Credential{
				Username: user,
				Password: password,
			},
		)
	}
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	err = client.Ping(ctx, nil)
	if err != nil {
		gojlog.Error(err)
		return err
	}
	_mongoDB = client.Database(dbName)
	return nil
}

// collection возвращает колекцию по имени из основной базы
func collection(name string) *mongo.Collection { return _mongoDB.Collection(name) }

// названия коллекций в монго
const (
	LogCollectionName = "logs"
)

// NewTimeoutContext создает контекст с таймаутом для базы
func NewTimeoutContext(parent context.Context) (context.Context, context.CancelFunc) {
	if parent == nil {
		parent = context.TODO()
	}
	return context.WithTimeout(parent, time.Minute)
}

// PrettyTime форматирует время в читабельный для пользователя вид в местной локации
func PrettyTime(t time.Time) string {
	if t.IsZero() {
		return ""
	}
	const baseLocation = "Asia/Krasnoyarsk"
	const prettyTimeFormat = "02.01.2006 15:04:05"
	l, err := time.LoadLocation(baseLocation)
	if err != nil {
		gojlog.Error(err)
		return "error"
	}
	return t.In(l).Format(prettyTimeFormat)
}
