package mongodb

import (
	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// MessageTypes структура для хранения информации о распределнии типов сообщения
type MessageTypes struct {
	Info     int64 `json:"info"`
	Warning  int64 `json:"warning"`
	Error    int64 `json:"error"`
	Critical int64 `json:"critical"`
	All      int64 `json:"all"`
}

// CalcMT считает распредление по типам сообщений
func CalcMT(f MessageFilter) (mt MessageTypes) {
	ctx, cnl := NewTimeoutContext(nil)
	defer cnl()
	filter, _ := f.toMongo()
	cursor, err := collection(LogCollectionName).Aggregate(ctx, mongo.Pipeline{
		{bson.E{
			Key:   "$match",
			Value: filter,
		}},
		{bson.E{
			Key: "$group",
			Value: bson.D{
				bson.E{Key: "_id", Value: "$level"},
				bson.E{Key: "count", Value: bson.M{"$sum": 1}},
			},
		}}})
	if err != nil {
		gojlog.Error(err)
		return
	}
	defer cursor.Close(ctx)
	type tmp struct {
		Name  string `bson:"_id"`
		Count int64  `bson:"count"`
	}
	for cursor.Next(ctx) {
		c := tmp{}
		if err := cursor.Decode(&c); err != nil {
			gojlog.Error(err)
			continue
		}
		if c.Name == "info" {
			mt.Info = c.Count
		}
		if c.Name == "warning" {
			mt.Warning = c.Count
		}
		if c.Name == "error" {
			mt.Error = c.Count
		}
		if c.Name == "critical" {
			mt.Critical = c.Count
		}
	}
	mt.All = mt.Info + mt.Warning + mt.Error + mt.Critical
	return
}
