package mongodb

import (
	"fmt"
	"time"

	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// LogMessage структура для сообщения лога
type LogMessage struct {
	Time    time.Time `json:"time" bson:"time"`       // время сообщения
	From    string    `json:"from" bson:"from"`       // место возникновения сообщения
	Author  string    `json:"author" bson:"author"`   // автор сообщения (программа)
	Level   string    `json:"level" bson:"level"`     // уровень сообщения
	Message string    `json:"message" bson:"message"` // сообщение
}

// CreateMessage создает в базе новую запись сообщения лога
func CreateMessage(m LogMessage) (id primitive.ObjectID, err error) {
	ctx, cnl := NewTimeoutContext(nil)
	defer cnl()
	res, err := collection(LogCollectionName).InsertOne(ctx, m)
	if err != nil {
		gojlog.Error(err)
		return id, err
	}
	id, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		err = fmt.Errorf("ошибка конвертирования ID: %+v", res.InsertedID)
		gojlog.Error(err)
		return id, err
	}
	return id, nil
}

// MessageFilter фильтр для получения списка сообщений лога
type MessageFilter struct {
	Time         TimeFilter `json:"time,omitempty"`
	From         TextFilter `json:"from,omitempty"`
	Author       TextFilter `json:"author,omitempty"`
	AuthorRegexp TextFilter `json:"author_regexp,omitempty"`
	Level        TextFilter `json:"level,omitempty"`
	Message      TextFilter `json:"message,omitempty"`
	Page         PageFilter `json:"page,omitempty"`
}

func (f MessageFilter) toMongo() (*bson.M, *options.FindOptions) {
	opt := f.Page.ToOption("time")
	filter := bson.M{}
	f.Time.Apply(filter, "time")
	f.From.Apply(filter, "from", true)
	f.Author.Apply(filter, "author", false)
	if f.Author == "" {
		f.AuthorRegexp.Apply(filter, "author", true)
	}
	f.Level.Apply(filter, "level", false)
	f.Message.Apply(filter, "message", true)
	return &filter, opt
}

// FilteredMessages получить данные о сообщениях лога по фильтру
func FilteredMessages(f MessageFilter) (messages []LogMessage, page PageInfo) {
	messages = make([]LogMessage, 0)
	filter, opt := f.toMongo()
	ctx, cnl := NewTimeoutContext(nil)
	defer cnl()
	// получение информации о количестве документов
	c, err := collection(LogCollectionName).CountDocuments(ctx, filter)
	if err != nil {
		gojlog.Error(err)
		return
	}
	// получение данных
	cursor, err := collection(LogCollectionName).Find(ctx, filter, opt)
	if err != nil {
		gojlog.Error(err)
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		m := LogMessage{}
		if err := cursor.Decode(&m); err != nil {
			gojlog.Error(err)
		} else {
			messages = append(messages, m)
		}
	}
	// расчет данных страницы
	page.Calc(f.Page.MaxCount, f.Page.Offset, c, int64(len(messages)))
	return
}
