package mongodb

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// PageFilter базовый фильтр с управлением количества и порядка элементов
type PageFilter struct {
	MaxCount int64 `json:"count,omitempty"`
	Offset   int64 `json:"offset,omitempty"`
	Reverse  bool  `json:"reverse,omitempty"`
}

// ToOption переводит параметры фильтра в опции для запроса монго
func (pf PageFilter) ToOption(sortFieldName string) *options.FindOptions {
	opt := options.Find()
	// switch заместо нескольких if
	// если строка пустая то просто игнорирует, иначе выставляет сортировку по полю
	switch {
	case sortFieldName == "":
	case pf.Reverse:
		opt.SetSort(bson.M{sortFieldName: -1})
	case !pf.Reverse:
		opt.SetSort(bson.M{sortFieldName: 1})
	}
	if pf.MaxCount > 0 {
		opt.SetLimit(int64(pf.MaxCount))
	}
	if pf.Offset > 0 {
		opt.SetSkip(int64(pf.Offset))
	}
	return opt
}

// TimeFilter фильтр для поля времени
// имеет возможность фильтровать по диапазону
type TimeFilter struct {
	Min time.Time `json:"min,omitempty"`
	Max time.Time `json:"max,omitempty"`
}

// ToBson конвертирует фильтр в данные для bson mongo
// перед присвоением полю нужна проверка на nil иначе возможно получить некоректный результат
func (f TimeFilter) ToBson() bson.M {
	exist := false
	filter := bson.M{}
	// если значение не пустое то выставляем фильтр
	if !f.Min.IsZero() {
		filter["$gte"] = f.Min
		exist = true
	}
	// если значение не пустое то выставляем фильтр
	if !f.Max.IsZero() {
		filter["$lte"] = f.Max
		exist = true
	}
	if exist {
		return filter
	}
	return nil
}

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
func (f TimeFilter) Apply(filter bson.M, name string) bool {
	if tf := f.ToBson(); tf != nil {
		filter[name] = tf
		return true
	}
	return false
}

// PageInfo информация о странице
// используется в случае возврата значений постранично
// Current - текущая страница
// Len - длина страницы (в элементах на страницу)
// Count - количество страниц
// Items - количество элементов на данной странице
// ItemsTotal - количество элементов всего
type PageInfo struct {
	Current    int64 `json:"current"`
	Len        int64 `json:"len"`
	Count      int64 `json:"count"`
	Items      int64 `json:"items"`
	ItemsTotal int64 `json:"items_total"`
}

// Calc расчитывает параметры страницы по данным фильтра
func (pi *PageInfo) Calc(count, offset, all, items int64) *PageInfo {
	// случай когда все данные выгружаются на 1 страницу (выгрузка без пагинации или ограничений)
	if count < 1 {
		pi.Current = 1
		pi.Len = all
		pi.Count = 1
		pi.Items = items
		pi.ItemsTotal = all
		return pi
	}
	// смещаем на первый элемент после сдвига
	offset++
	pi.Current = offset / count
	if offset%count > 0 {
		pi.Current++
	}
	pi.Len = count
	pi.Count = all / count
	if all%count > 0 || all == 0 {
		pi.Count++
	}
	pi.Items = items
	pi.ItemsTotal = all
	return pi
}

// TextFilter фильтр для поиска текста
type TextFilter string

// Apply применяет фильтр
// filter объект фильтра
// name название поля в bson
// regexp поиск по регулярному выражению (без учета регистра)
func (f TextFilter) Apply(filter bson.M, name string, regexp bool) bool {
	if f == "" {
		return false
	}
	if regexp {
		filter[name] = bson.M{
			"$regex":   f,
			"$options": "i",
		}
	} else {
		filter[name] = f
	}
	return true
}
