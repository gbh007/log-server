package mongodb

import (
	"gitlab.com/gbh007/gojlog"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// MessageFrom структура для хранения информации о распределнии места возникновения сообщения
type MessageFrom map[string]int64

// CalcMF считает распредление по местам возникновения сообщений
func CalcMF(f MessageFilter) (ma MessageFrom) {
	ma = make(MessageFrom)
	ctx, cnl := NewTimeoutContext(nil)
	defer cnl()
	filter, _ := f.toMongo()
	cursor, err := collection(LogCollectionName).Aggregate(ctx, mongo.Pipeline{
		{bson.E{
			Key:   "$match",
			Value: filter,
		}},
		{bson.E{
			Key: "$group",
			Value: bson.D{
				bson.E{Key: "_id", Value: "$from"},
				bson.E{Key: "count", Value: bson.M{"$sum": 1}},
			},
		}}})
	if err != nil {
		gojlog.Error(err)
		return
	}
	defer cursor.Close(ctx)
	type tmp struct {
		Name  string `bson:"_id"`
		Count int64  `bson:"count"`
	}
	for cursor.Next(ctx) {
		c := tmp{}
		if err := cursor.Decode(&c); err != nil {
			gojlog.Error(err)
			continue
		}
		ma[c.Name] = c.Count
	}
	return
}
